﻿using System;

/*plokikommen-
 taar 
 
     */
namespace EsimeneKonsool
{
    class Program
    //Reakomentaar. Sulgude treppimine
    // VS värvib sõnu
    // sulud eraldi reale
    {
        static void Main()
        /// <summary>
        /// kolm kaldkriipsu. tekst genereerib Documentatsiooni
        /// </summary>
        { 
            Console.WriteLine("Tere wolrd!");
            // Muutuja - andmetüüp
         
        }

    class Prograam
        {
            static void Main()
            {
                bool res;
                int a;
                string myStr = "12";
                res = int.TryParse(myStr, out a);
                Console.WriteLine("String is a numeric representation: " + res);
            }
        }

    }
    //*Switchiv kommentar. Liitkommentaar
    // Nippets
}
//Document block

//koodiplokke ajutiselt välja lülitada. Välja kommenteerimine


// andmetüüp - defineerib nelja asjandust
//1.Kui palju bitte vaja on - maht
//2.mida bitid tähendavad - semantika
//3.Väärtuse hulk - süntaks, domain
//4.Mida nendega teha saab - operatsioonid, tehted ,jms.

//int
//sinine võtmesõna
//Int32;
// Roheline on klass


//Klasside nimed suure tähega
//Võtmesõnad on väikese tähega


// avaldis - on kokku pandud
//literaaldi - arvud ja muud "konstandid"
//muutujatest
//tehetest
//sulgudest
//funktsioonidest

#region kommenteerisin välja


//protsent 10 % 3


//binaarsed tehted  + / - * % liittehtemärgid += -= *=
//unaarsed tehted ! -
//ternaarne tehe - kolm operand
//   boolean avaldis ? jah-väärtus: ei-väärtus (Elvise tehe)

// = omistamine
// == võrdlemistehe

//a++ liidetakse viimasel väärtusele
//++a  kasutatakse arvu viimast väärtust

// & jaa-tehe
// | või - tehe

String nimi = "Hannes Verlis";
    Console.WriteLine(nimi[1]+0);



   // @ - unescaped string

    //jutumärk \"kui arno\" jutumärk escapetud stringis
    //  /n - linefeed märk
    //  /r - carriage return

    // string on MUUTUMATU



    //var
    // Console.WriteLine(henn.GetType().Name)


    //

    // arvusid saab teisteks arvudeks teisendada
    int i = 1000;
    long l = 1;  // toimub ilmutamaata tüübiteisendus e. casting


